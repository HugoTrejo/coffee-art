//function with JS to load google map VHTO
;(function () {

	class UserLocation{//class for current user location
		static get(callback){
			if(navigator.geolocation){//if browser supports geolocation
				navigator.geolocation.getCurrentPosition((location)=>{
					callback({//current user geolocation
						lat: location.coords.latitude,
						lng: location.coords.longitude
					})
				})
			}else{
				alert("No pudimos detectar ubicación actual debido a versión del navegador")
				// return {
				// 	lat: 0,
				// 	lng: 0
				// }
			}
		}
	}

	const my_place ={
		lat:19.4248097,
		lng:-99.19492559999998
	}

	google.maps.event.addDomListener(window, "load", ()=>{
		const map = new google.maps.Map(
			document.getElementById('map'),
			{
				center:my_place,//point of reference
				zoom:15
			}
		)

		const marker = new google.maps.Marker({//Adds google map marker with JSON config
			map: map,
			position: my_place,
			title: "Coffe-Art",
			visible: true

		})
		//calls method get of class UserLocation
		UserLocation.get((coords)=>{
			//calculate the distance between reference point and user geolocation
			let origen = new google.maps.LatLng(coords.lat, coords.lng)//LatLng
			let destino = new google.maps.LatLng(my_place.lat, my_place.lng)
			//console.log(coords)//shows the user position in the console
		
			let service = new google.maps.DistanceMatrixService()

			service.getDistanceMatrix({
				origins:[origen],
				destinations:[destino],
				travelMode: google.maps.TravelMode.DRIVING //Defining travel mode in map routes as driving a car
			},(response, status)=>{
				if(status === google.maps.DistanceMatrixStatus.OK){
					const duration_element = response.rows[0].elements[0]
					const duracion_viaje = duration_element.duration.text
					document.querySelector("#message")//shows travel duration in the div #message, using string ``
							.innerHTML = `
							Ud. está a ${duracion_viaje} de degustar un buen arte hecho café
							`
					console.log(duration_element)
				}
			})
		})


	})
})()