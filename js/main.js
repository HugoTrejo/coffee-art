// //Validates service worker for browsers in order to work with offline configuration
// if(navigator.serviceWorker){
// 	navigator.serviceWorker.register("/coffee/sw.js") //register Service Worker
// }

//function for responsive menu show on click with JQuery
$("#menu-opener").on("click", function(){
	$("#responsive-nav ul").toggleClass("active") //switch class active
	$(this).toggleClass("glyphicon-menu-hamburger")//switch icon class for different icon 
})



//Main function for sticky navigation VHTO
;(function(){

	let sticky = false //indicator
	let currentPosition = 0 // var for know the current position of the image slider gallery
	const imageCounter = $("[data-name='image-counter']").attr("content") //metadata for number of images in the gallery slider
	const email = "hugotrejo57@gmail.com" //const for email to send messages via formspree



	$("#contact-form").on("submit", function(ev){ //adds listener to submit event for call function send form information
		ev.preventDefault()

		sendForm($(this))
		return false;
	})


	isOpen()//calls isOpen fuction 


//Function for gallery image slider
	setInterval(()=>{//3000ms interval for slider

		if(currentPosition < imageCounter){//validates the current position of slider
			currentPosition++//Adds 1 position
		}else{
			currentPosition = 0 //Restart current position
		}


		$("#gallery .inner").css({
			left:"-"+currentPosition*100+"%"
		})
	},3000)



	//Monitoring the scroll with an indicator "true or false" of the function isInBottom()
	$(window).scroll(function(){
		const inBottom  = isInBottom()

		if(inBottom && !sticky){ //validates 
			//change navigation
			sticky = true
			stickNavigation()
			console.log("Cambiar navegación")			
		}
		else if(!inBottom && sticky){//validation
			sticky = false
			unStickNavigation()
			console.log("Regresar navegación")
		}
	})

	function isOpen(){//function to know the coffee schedule
		//clock in 24 hrs 7.00hrs - 1.00hrs
		let date = new Date()
		const current_hour = date.getHours()

		if(current_hour < 7 && current_hour > 1){
			console.log("cerrado")
			$("#is-open .text").html("Cerrado ahora <br> Abierto de 7:00am a 1:00am")//change text in html div #is-open
		}else{
			console.log("abierto")
		}
	}
//Different js functions for sticky navigation
	function stickNavigation(){
		//Adds and removes classes to diferent div´s classes
		$("#description").addClass("fixed").removeClass("absolute")
		$("#navigation").addClass("hidden")
		$("#sticky-navigation").removeClass("hidden")
	}

	function unStickNavigation(){
		//Adds and removes classes to diferent div´s classes
		$("#description").removeClass("fixed").addClass("absolute")
		$("#navigation").removeClass("hidden")
		$("#sticky-navigation").addClass("hidden")
	}

	function isInBottom(){
		//validates the position of scrolling
		const $description = $("#description")
		const descriptionHeight = $description.height()

		return $(window).scrollTop() > $(window).height() - (descriptionHeight * 1.7)
	}



})()
//()executes the main function of js