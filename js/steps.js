//Main function for steps form
;(function(){

	//$(".step:nth-child(1)").addClass("active") //adds CSS class active to de div .step

	const selector ="#contact-form"


	$(".path-step").on("click", (ev)=>{//listener on click to move the form fields with circles 
		const $current_circle = $(ev.target)

		focus_circle($current_circle)

		const posicion = $current_circle.index(".path-step") + 1

		let $test = $(".step:nth-child("+posicion+")")
		siguiente($test)

	})


	$(selector).find(".input").on("change", (ev)=>{
		const $input = $(ev.target)

		const $next_step = $input.parent().next(".step")

		const is_form_valid = es_valido_formulario()

		if(!is_form_valid && $next_step.length > 0){

			siguiente($next_step)

		}else{
			validar_formulario()
		}

		
	})

	//Helpers

	function validar_formulario(){
		if(es_valido_formulario()){
			enviar_formulario()

		}else{
			let $fieldset_invalido = $(selector).find(".input:invalid").first().parent()
			siguiente($fieldset_invalido)
		}
	}

	function es_valido_formulario(){
		return document.querySelector(selector).checkValidity()
	}

	function siguiente($next_step){
		$(".step.active").removeClass("active")
		$next_step.addClass("active")
		$next_step.find(".input").focus()

		//Coordinar circulos
		const posicion =( $next_step.index(".step")) + 1

		const $circle = $(".path-step:nth-child("+posicion+")")
		
		focus_circle($circle)
		//validar_formulario()
		//$next_input.focus()
	}

	function focus_circle($circle){
		$(".path-step.active").removeClass("active")
		$circle.addClass("active")
	}

	//Sends data to mailer service formspree via ajax using JQuery
	function enviar_formulario(){
		const $form = $(selector)
		$.ajax({
		    url: $form.attr("action"), //"https://formspree.io/hugotrejo57@gmail.com", 
		    method: "POST",
		    data: $form.formObject(),
		    dataType: "json",
		    success: function(){
		    	alert("Todo salió bien")
		    }
		})
	}


})()